/* eslint-disable func-names,no-var,prefer-reflect,prefer-arrow-callback */
(function () {
    var win = window; // eslint-disable-line no-undef
    var FR = win.FileReader;
    var doc = win.document;
    var kjua = win.kjua;

    var gui_val_pairs = [
    ];

    function el_by_id(id) {
        return doc.getElementById(id);
    }

    function val_by_id(id) {
        var el = el_by_id(id);
        return el && el.value;
    }

    function on_event(el, type, fn) {
        el.addEventListener(type, fn);
    }

    function on_ready(fn) {
        on_event(doc, 'DOMContentLoaded', fn);
    }

    function for_each(list, fn) {
        Array.prototype.forEach.call(list, fn);
    }

    function all(query, fn) {
        var els = doc.querySelectorAll(query);
        if (fn) {
            for_each(els, fn);
        }
        return els;
    }

    function update_qrcode() {
        var options = {
            render: 'canvas',
            ecLevel: 'H',
            minVersion: 1,

            fill: '#333333',
            back: '#ffffff',

            text: val_by_id('text'),
            size: 400,
            rounded: 100,
            quiet: 1,

            mode: 'label',

            mSize: 20,
            mPosX: 50,
            mPosY: 50,

            label: '',
            fontname: 'Ubuntu Mono',
            fontcolor: '#ff9818',
        };
        var cr_address = val_by_id('address');
        var currency = val_by_id('currency');
        var amount = val_by_id('amount');
        var crypto_erc_address = val_by_id('crypto_erc_address');
        var currency_amount_in = val_by_id('currency_amount_in');

        el_by_id('text').value = currency+'://'+cr_address+'?'+currency_amount_in+'='+amount;

        if( currency == 'ether' ) {
            el_by_id('text').value = currency+'eum:'+crypto_erc_address+'/transfer?address='+cr_address+'&uint256='+amount;
        }

        var container = el_by_id('container');
        var qrcode = kjua(options);
        for_each(container.childNodes, function (child) {
            container.removeChild(child);
        });
        if (qrcode) {
            container.appendChild(qrcode);
        }
    }

    function update() {
        update_qrcode();
    }

    function on_img_input() {
        var input = el_by_id('image');
        if (input.files && input.files[0]) {
            var reader = new FR();
            reader.onload = function (ev) {
                el_by_id('img-buffer').setAttribute('src', ev.target.result);
                el_by_id('mode').value = 4;
                setTimeout(update, 250);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    on_ready(function () {
        on_event(el_by_id('image'), 'change', on_img_input);
        all('input, textarea, select', function (el) {
            on_event(el, 'input', update);
            on_event(el, 'change', update);
            on_event(el, 'click', update);
        });
        on_event(win, 'load', update);
        update();
    });
}());
/* eslint-enable */

(function( $ ) {
	'use strict';
        $(document).ready(function(){
        var ca_order_id = $(document).find('#ca_order_id').val();
        var ca_data = {
            'action': 'cryptapi_order_status',
            'order_id' : ca_order_id
        };
        var ca_data_confirm = {
            'action': 'cryptapi_order_status',
            'order_id' : ca_order_id,
            'check_pay_cnfrm' : 'yes'
        };
        if( $(document).find('#cryptapi_payment_made_half').val() == 'done' ) {

            $(document).find('.loader-text-change').text('partial payment received, kindly make remaining payment to confirm the order');
            $(document).find('.hide_when_partial').hide();
        }
        if( $(document).find('#cryptapi_payment_successful').val() == 'done' ) {

            $(document).find('.loader').addClass('ca_right_hide');
            $(document).find('.loader-text').addClass('ca_right_hide');
            $(document).find('#container').addClass('ca_right_hide');
            $(document).find('.ca_payment_confirm').removeClass('ca_right_hide');
        }
        setInterval(function(){ca_payment_verification(ca_data);}, 1000);
        setInterval(function(){ca_payment_confirmation(ca_data_confirm);}, 30500);
    })
    function ca_payment_verification(ca_data,) {
        $.post(ca_ajax_object.ca_ajax_url, ca_data, function(response) {
            
            var json_response = JSON.parse(response);
            if( json_response['is_paid'] == true )
            {
                $(document).find('.loader').addClass('ca_right_hide');
                $(document).find('.loader-text').addClass('ca_right_hide');
                $(document).find('#container').addClass('ca_right_hide');
                $(document).find('.ca_payment_confirm').removeClass('ca_right_hide');
            }
            else if( json_response['is_timeout'] != '' && json_response['is_timeout'] == 'yes' ) {
                
                $(document).find('.loader-text-change').text('We are still waiting for payment, Kindly make payment to confirm your order');
            }
            else {
                var now = json_response['date'];
                var countDownDate = json_response['formatdate'];

                now = now + 1000;
                
                var distance = countDownDate - now;
                var days = Math.floor(distance / (1000 * 60 * 60 * 24));
                var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
                var seconds = Math.floor((distance % (1000 * 60)) / 1000);
                
                $(document).find('#demo').text(hours + " Hours " + minutes + " Minutes & " + seconds + " Seconds "); 
        
                if (distance < 0) {
                    // clearInterval(x);
                    // $(document).find('#demo').text("EXPIRED"); 
                }
            }
        });
    }
    function ca_payment_confirmation( ca_data_confirm ) {
        $.post(ca_ajax_object.ca_ajax_url, ca_data_confirm, function(response) {

        });
    }
})( jQuery );
