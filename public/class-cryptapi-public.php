<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://cryptapi.io/
 * @since      1.0.0
 *
 * @package    Cryptapi
 * @subpackage Cryptapi/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Cryptapi
 * @subpackage Cryptapi/public
 * @author     cryptapi <developer@cryptapi.io>
 */
class Cryptapi_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

    public static $COIN_MULTIPLIERS = [
        'btc' => 100000000,
        'bch' => 100000000,
        'ltc' => 100000000,
        'eth' => 1000000000000000000,
        'trx' => 1000000000000000000,
        'usdt' => 10000000,
        'usdc' => 10000000,
        'busd' => 10000000000000000000,
        'pax' => 10000000000000000000,
        'tusd' => 10000000000000000000,
        'bnb' => 10000000000000000000,
        'link' => 10000000000000000000,
        'cro' => 1000000000,
        'mkr' => 10000000000000000000,
        'nexo' => 10000000000000000000,
        'bcz' => 10000000000000000000,
        'iota' => 1000000,
        'trx' => 1000000,
        'xmr' => 1000000000000,
    ];

	public $ERC_TOKENS_URI = [
        'usdt' => '0xdAC17F958D2ee523a2206206994597C13D831ec7',
        'usdc' => '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48',
        'busd' => '0x4Fabb145d64652a948d72533023f6E7A623C7C53',
        'pax' => '0x8E870D67F660D95d5be530380D0eC0bd388289E1',
        'tusd' => '0x0000000000085d4780B73119b644AE5ecd22b376',
        'bnb' => '0xB8c77482e45F1F44dE1745F52C74426C631bDD52',
        'link' => '0x514910771AF9Ca656af840dff83E8264EcF986CA',
        'cro' => '0xA0b73E1Ff0B80914AB6fe0444E65848C4C34450b',
        'mkr' => '0x9f8F72aA9304c8B593d555F12eF6589cC3A579A2',
        'nexo' => '0xB62132e35a6c13ee1EE0f84dC5d40bad8d815206',
        'bcz' => '0x08399ab5eBBE96870B289754A7bD21E7EC8c6FCb',
    ];

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

		ini_set('display_errors', 1);
		ini_set('display_startup_errors', 1);
		error_reporting(E_ALL);

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Cryptapi_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Cryptapi_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/cryptapi-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Cryptapi_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Cryptapi_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/cryptapi-public.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * The Function is used to display QR and order details on the Thank you page.
	 *
	 * @since    1.0.0
	 */
	function cryptapi_thankyou_page( $order_id ) {

		$order = new WC_Order( $order_id );
        $total = $order->get_total();
        $currency_symbol = get_woocommerce_currency_symbol();
        $address_in = $order->get_meta('cryptapi_address');
        $crypto_value = $order->get_meta('cryptapi_total');
        $cryptapi_payment_done_partially = $order->get_meta('cryptapi_payment_done_partially');
		
		if( isset( $cryptapi_payment_done_partially ) && !empty( $cryptapi_payment_done_partially ) ) {
			// print_r($cryptapi_payment_done_partially); die;
			$crypto_value = $cryptapi_payment_done_partially;
			?>
				<input type='hidden' id='cryptapi_payment_made_half' value='done'>
			<?php
		}
        $crypto_coin = $order->get_meta('cryptapi_currency');
        $crypto_coin_full_name = $order->get_meta('cryptapi_currency_full_name');
        $currency_uri_first = $order->get_meta('currency_uri_first');
		$currency_amount_in = 'amount';
		
		$show_crypto_coin = $crypto_coin;
        if ($show_crypto_coin == 'iota') $show_crypto_coin = 'miota';

		$qr_value = $crypto_value;
        if (in_array( $crypto_coin, array('eth', 'iota') ) ) $qr_value = $this->convert_mul( $crypto_value, $crypto_coin );

		if (in_array( $crypto_coin, array('usdt', 'usdc', 'busd', 'pax', 'tusd', 'bnb', 'link', 'cro', 'mkr', 'nexo', 'bcz' ) ) ) {
			$qr_value = $this->convert_mul( $crypto_value, $crypto_coin );
			$crypto_erc_address = $this->ERC_TOKENS_URI[$crypto_coin];
			$currency_uri_first = 'ether';
	   	}
		
		if( $crypto_coin == 'bch' ) {
			if (strpos($address_in, $currency_uri_first.':') !== false) {
				$address_in = str_replace($currency_uri_first.':','',$address_in);
			}
		}
		if( $crypto_coin == 'xmr' ) {

			$currency_amount_in = 'tx_amount';
		}
		if( $crypto_coin == 'eth' ) {
			$qr_value = number_format($qr_value);
			if (strpos($qr_value, ',') !== false) {
				$qr_value = str_replace(',','',$qr_value);
			}
			$currency_amount_in = 'value';
		}

		if( $qr_value > 1000 ) {

			$qr_value = number_format($qr_value);
			if (strpos($qr_value, ',') !== false) {
				$qr_value = str_replace(',','',$qr_value);
			}
		}
		
		wp_enqueue_style( 'thank-you-page', plugin_dir_url( __FILE__ ) . 'css/cryptapi-thankyou.css', array(), $this->version, 'all' );
		wp_enqueue_script( 'qr-code-min', plugin_dir_url( __FILE__ ) . 'js/kjua-0.9.0.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( 'qr-code-script', plugin_dir_url( __FILE__ ) . 'js/scripts.js', array( 'jquery' ), $this->version, false );
		wp_localize_script( 'qr-code-script', 'ca_ajax_object', array( 'ca_ajax_url' => admin_url( 'admin-ajax.php' ) ) );

		$cryptapi_payment_successful = $order->get_meta('cryptapi_payment_successful');
			if( isset( $cryptapi_payment_successful ) && !empty( $cryptapi_payment_successful ) ) {
				if( $cryptapi_payment_successful  == 'done' ) {
					?>
						<input type='hidden' id='cryptapi_payment_successful' value='done'>
					<?php
				}
			}
		?>
		<input type='hidden' id='ca_order_id' value='<?php echo $order_id; ?>' />
		<div class='loader-center'><div class="loader"></div></div>
		<img src='<?php echo plugin_dir_url( __FILE__ ). 'img/check.png' ?>' class='ca_payment_confirm ca_right_hide'/>
		<h4 class='ca_payment_confirm ca_right_hide' style="width: 100%; margin: 2rem auto; text-align: center;"><?php echo __('Your payment has been confirmed!', 'cryptapi'); ?></h4>
		<img src='<?php echo plugin_dir_url( __FILE__ ). 'img/cancel.png' ?>' class='ca_payment_cancel ca_right_hide'/>
		<h4 class='ca_payment_cancel ca_right_hide' style="width: 100%; margin: 2rem auto; text-align: center;"><?php echo __('Your payment has not been confirm yet, order status send back to '.$order->get_status().' mode', 'cryptapi'); ?></h4>
		<h4 class='text-margin loader-text-change loader-text'><?php esc_html_e( 'Waiting for payment - ' ); ?><span id='demo'></span><span><?php esc_html_e( ' Remaining' ); ?></span></h4>
		<div id="container" class='ca_slite_container'>
			<div class='ca_right_hide'>
				<textarea class='ca_right_hide' id="text"></textarea>
				<input type='hidden' class='ca_right_hide' id="address" value='<?php echo $address_in; ?>' />
				<input class='ca_right_hide' type='hidden' id="currency" value='<?php echo $currency_uri_first; ?>' />
				<input class='ca_right_hide' type='hidden' id="currency_amount_in" value='<?php echo $currency_amount_in; ?>' />
				<input class='ca_right_hide' type='hidden' id="crypto_erc_address" value='<?php echo $crypto_erc_address; ?>' />
				<input class='ca_right_hide' type='hidden' id="amount" value='<?php echo $qr_value; ?>' />
			</div>
			<div class="ca_right_hide">
				<input class="ca_right_hide" id="image" type="file" />
			</div>
		</div>
		<div class='loader-text' style="width: 100%; margin: 2rem auto; text-align: center;">
			<?php echo __('In order to confirm your order, please send', 'cryptapi') ?>
			<span style="font-weight: 500"><?php echo $crypto_value ?></span>
			<span style="font-weight: 500"><?php echo strtoupper($show_crypto_coin) ?></span>
			<span class='hide_when_partial'>(<?php echo $currency_symbol . ' ' . $total; ?>)</span>
			<?php echo __('to', 'cryptapi') ?>
			<span style="font-weight: 500"><?php echo $address_in ?></span>
		</div>
		<?php
	}

	/**
	 * The Function is used to convert coins & values multiple.
	 *
	 * @since    1.0.0
	 */
	private function convert_mul( $val, $coin ) {
        return $val * Cryptapi_Public::$COIN_MULTIPLIERS[$coin];
    }

	/**
	 * The Function is used verify payment on the callback url of thank you page
	 *
	 * @since    1.0.0
	 */
	function validate_payment( $payment_cb, $order_id, $nonce ) {

		$data = $this->process_callback( $payment_cb );

		$order = new WC_Order($order_id);

		if ($order->is_paid() || $nonce != $order->get_meta('cryptapi_nonce')) die("*ok*");
		
        $value_convert = $this->convert_div($data['value'], $data['coin']);
        $paid = floatval($order->get_meta('cryptpi_paid')) + $value_convert;

		if( isset( $data['callbacks'] ) && is_array( $data['callbacks'] ) && !empty( $data['callbacks'] ) ) {

			if( count( $data['callbacks'] ) > 1 ) {

				if ($paid >= $order->get_meta('cryptapi_total')) {

					$order->payment_complete($data['txid_in']);
					$order->add_meta_data('cryptapi_payment_successful', 'done');
					$order->delete_meta_data('cryptapi_payment_done_partially');
				}
				else
				{
					$amount_remaining = $order->get_meta('cryptapi_total') - $paid;
					$order->add_meta_data('cryptapi_payment_done_partially', $amount_remaining);
				}
			}
		}

        $order->save_meta_data();
        die("*ok*");
	}

	/**
	 * The Function contains the Get parameters of the order confirmation
	 *
	 * @since    1.0.0
	 */
	function process_callback($_get, $convert = false) {
		
		$params = [
            'address_in' => $_get['address_in'],
            'address_out' => $_get['address_out'],
            'callbacks' => $_get['callbacks'],
            'txid_in' => isset($_get['callbacks']['txid_in']) ? $_get['callbacks']['txid_in'] : null,
            'txid_out' => isset($_get['callbacks']['txid_out']) ? $_get['callbacks']['txid_out'] : null,
            'confirmations' => isset($_get['callbacks']['confirmations']) ? $_get['callbacks']['confirmations'] : null,
            'value' => isset( $_get['callbacks']['value'] ) ? $convert ? $this->convert_div($_get['callbacks']['value'], $_get['callbacks']['value_coin']) : $_get['callbacks']['value'] : '',
            'value_forwarded' => isset($_get['callbacks']['value_forwarded']) ? ($convert ? $this->convert_div($_get['callbacks']['value_forwarded'], $_get['callbacks']['value_coin']) : $_get['callbacks']['value_forwarded']) : null,
            'coin' => isset($_get['callbacks']['value_coin']) ? $_get['callbacks']['value_coin'] : null,
            'pending' => isset($_get['notify_pending']) ? $_get['notify_pending'] : false,
        ];
        
        foreach ($params as &$val) {
            $val = sanitize_text_field($val);
        }

        return $params;
	}

	/**
	 * The Function is used to convert coins & values division.
	 *
	 * @since    1.0.0
	 */
	private function convert_div( $val, $coin ) {
		if( !empty($val) && !empty( $coin ) ) {
			return $val / Cryptapi_Public::$COIN_MULTIPLIERS[$coin];
		}
    }

	/**
	 * The Function used to send ajax every second to confirm that payment has been made or not
	 *
	 * @since    1.0.0
	 */
	function ca_order_status() {
		
		if( isset( $_POST['order_id'] ) && !empty( $_POST['order_id'] ) ) {

			$order_id = sanitize_text_field($_POST['order_id']);

			$order = new WC_Order($order_id);

			if( isset( $_POST['check_pay_cnfrm'] ) ) {
				if( $_POST['check_pay_cnfrm'] == 'yes' ) {
					
					$verify_callback_url = $order->get_meta('verify_callback_url');
					$prefix_url = $order->get_meta('prefix_url');
					$cryptapi_nonce = $order->get_meta('cryptapi_nonce');
					$prefix_url = str_replace("create","logs",$prefix_url);
					$verify_cb_url = "{$prefix_url}?callback={$verify_callback_url}";
					$payment_cb = $this->payment_confirmation( $verify_cb_url );
					$this->validate_payment( $payment_cb, $order_id, $cryptapi_nonce );
				}
			}

			$data = [
				'is_paid' => $order->is_paid(),
				'is_pending' => boolval($order->get_meta('cryptapi_pending')),
			];

			$payment_timeout = get_post_meta( $order_id, 'payment_timeout', true );
				
			if( isset( $payment_timeout ) ) {
				$order_post = get_post( $order_id );
				$formatDate = $this->ca_format_date( $payment_timeout, strtotime( $order_post->post_date ) );
				$date = time();

				$time_data = [
					'formatdate' => strtotime( "$formatDate" ) * 1000,
					'date' => $date * 1000,
				];

				$data = array_merge( $data, $time_data );

				if( ( strtotime( $formatDate ) - $date ) <= 0 ) {

					$timeout_data = [
						'is_timeout' => 'yes',
					];
					$data = array_merge( $data, $timeout_data );

					$update_order_req = get_post_meta( $order_id, 'update_order_req', true );
					str_replace( 'wc-', '', $update_order_req );
					$order->update_status( $update_order_req );
					$order->delete_meta_data( 'update_order_req' );
					$order->delete_meta_data( 'payment_timeout' );
					$order->save_meta_data();
				}
			}	

			echo json_encode($data);
			die();
		}
		else {
			echo json_encode(['status' => 'error', 'error' => 'not a valid order_id']);
			die();
		}
	}

	/**
	 * The Function used to check wheather payment is done or not
	 *
	 * @since    1.0.0
	 */
	function payment_confirmation( $verify_cb_url ) {

		$ca_curl = curl_init();
        curl_setopt( $ca_curl, CURLOPT_HEADER, false );
        curl_setopt( $ca_curl, CURLOPT_HTTPHEADER, array( 'Accept: application/json', 'Accept-Language: en_US' ) );
        curl_setopt( $ca_curl, CURLOPT_VERBOSE, 1 );
        curl_setopt( $ca_curl, CURLOPT_TIMEOUT, 30 );
        curl_setopt( $ca_curl, CURLOPT_URL, $verify_cb_url );
        curl_setopt( $ca_curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ca_curl, CURLOPT_SSLVERSION, 6 );
        $ca_curl_response = curl_exec( $ca_curl );
        curl_close( $ca_curl );
		$ca_curl_response_array = json_decode( $ca_curl_response, true );
		return $ca_curl_response_array;
	}

	/**
	 * The Function used to update date format
	 *
	 * @since    1.0.0
	 */
	function ca_format_date( $payment_timeout, $post_date ) {

		if (strpos($payment_timeout, 'hr') !== false) {
			$payment_timeout = substr($payment_timeout, 0, -2);
			$futureDate = $post_date+(60*60*$payment_timeout);
			$formatDate = date("Y-m-d H:i:s", $futureDate);
			return $formatDate;
		}
		else if (strpos($payment_timeout, 'min') !== false) {
			$payment_timeout = substr($payment_timeout, 0, -3);
			$futureDate = $post_date+(60*$payment_timeout);
			$formatDate = date("Y-m-d H:i:s", $futureDate);
			return $formatDate;
		}
	}

}
