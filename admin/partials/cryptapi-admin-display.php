<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://cryptapi.io/
 * @since      1.0.0
 *
 */
?>

<?php

class WC_CryptAPI_Gateway extends WC_Payment_Gateway
{
    public $cryptapi_coin_options = array(
        'btc' => 'Bitcoin',
        'bch' => 'Bitcoin Cash',
        'ltc' => 'Litecoin',
        'eth' => 'Ethereum',
        'usdt' => 'USDT (ERC-20)',
        'usdc' => 'USDC (ERC-20)',
        'busd' => 'BUSD (ERC-20)',
        'pax' => 'PAX (ERC-20)',
        'tusd' => 'TUSD (ERC-20)',
        'bnb' => 'BNB (ERC-20)',
        'link' => 'ChainLink (ERC-20)',
        'cro' => 'Crypto Coin (ERC-20)',
        'mkr' => 'Maker (ERC-20)',
        'nexo' => 'NEXO (ERC-20)',
        'bcz' => 'BECAZ (ERC-20)',
        'xmr' => 'Monero',
        'iota' => 'IOTA',
        'trx' => 'TRX(TRC-20)',
    );

    private $erc20_tokens = array('usdt', 'usdc', 'busd', 'pax', 'tusd', 'bnb', 'link', 'cro', 'mkr', 'nexo', 'bcz');

    private $payment_timeout_values  = array(
        '' => 'Select Timeout',
        '5min' => '5 Minutes',
        '15min' => '15 Minutes',
        '30min' => '30 Minutes',
        '1hr' => '1 Hour',
        '2hr' => '2 Hour',
        '12hr' => '12 Hour',
        '24hr' => '24 Hour'
    );

    public static $COIN_MULTIPLIERS = [
        'btc' => 100000000,
        'bch' => 100000000,
        'ltc' => 100000000,
        'eth' => 1000000000000000000,
        'trx' => 1000000000000000000,
        'usdt' => 10000000,
        'usdc' => 10000000,
        'busd' => 10000000000000000000,
        'pax' => 10000000000000000000,
        'tusd' => 10000000000000000000,
        'bnb' => 10000000000000000000,
        'link' => 10000000000000000000,
        'cro' => 1000000000,
        'mkr' => 10000000000000000000,
        'nexo' => 10000000000000000000,
        'bcz' => 10000000000000000000,
        'iota' => 1000000,
        'trx' => 1000000,
        'xmr' => 1000000000000,
    ];

    function __construct()
    {
        $this->id = 'cryptapi';
        $this->has_fields = true;
        $this->method_title = 'CryptAPI';
        $this->method_description = 'CryptAPI allows customers to pay in cryptocurrency';
        $this->initiliaze_url = '';
        $this->init_form_fields();
        $this->init_settings();
        $this->cryptapi_settings();

        add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
    }

    /**
	 * WooCommerce init fields for registering the data-fields into the payment gateway section
	 *
	 * @since    1.0.0
	 */
    function init_form_fields()
    {
        $this->form_fields = array(
            'enabled' => array(
                'title' => esc_html__('Enabled', 'cryptapi'),
                'type' => 'checkbox',
                'label' => esc_html__('Enable CryptAPI Payments', 'cryptapi'),
                'default' => 'yes'
            ),

            'title' => array(
                'title' => esc_html__('Title', 'cryptapi'),
                'type' => 'text',
                'description' => esc_html__('This controls the title which the user sees during checkout.', 'cryptapi'),
                'default' => esc_html__('Cryptocurrency', 'cryptapi'),
                'desc_tip' => true,
            ),
            
            'description' 	=> array(
                'title'       	=> esc_html__('Description', 'cryptapi'),
                'type'        	=> 'textarea',
                'default'     	=> esc_html__('Pay with cryptocurrency (BTC, BCH, LTC, ETH, ERC-20, TRC-20, Monero, TRX, IOTA', 'cryptapi'),
                'description' 	=> esc_html__('Payment method description that the customer will see on your checkout', 'cryptapi' )
            ),

            'sandbox_mode' => array(
                'title' => esc_html__('Sandbox Mode', 'cryptapi'),
                'type' => 'checkbox',
                'class' => 'enable_sandbox',
                'label' => esc_html__('Enable Sandbox Mode, That will disable all the coins and switch you to BTC Testnet', 'cryptapi'),
                'default' => 'no'
            ),

            'sandbox_warning' => array(
                'title' => esc_html__('Warning : ', 'cryptapi'),
                'type' => 'checkbox',
                'class' => 'sandbox_warning_class',
                'label' => esc_html__('Enabling Sandbox Mode will disable all the real payment and switch you to BTC Testnet', 'cryptapi'),
                'default' => 'no'
            ),

            'coins' => array(
                'title' => esc_html__('Accepted cryptocurrencies', 'cryptapi'),
                'type' => 'multiselect',
                'default' => '',
                'class' => 'cryptapi_multiselect',
                'css' => 'height: 10rem;',
                'options' => $this->cryptapi_coin_options,
                'description' => esc_html__("Select which coins do you wish to accept. CTRL + click to select multiple", 'cryptapi'),
            ),

            'btc_test_address' => array(
                'title' => esc_html__('Bitcoin Testnet Address', 'cryptapi'),
                'type' => 'text',
                'class' => 'btc_test_address ca_test_crypto_hide',
                'description' => esc_html__("Insert your Bitcoin Testnet address here. Leave blank if you want to skip this cryptocurrency", 'cryptapi'),
                'desc_tip' => true,
            ),

            'btc_address' => array(
                'title' => esc_html__('Bitcoin Address', 'cryptapi'),
                'type' => 'text',
                'class' => 'btc_address ca_crypto_hide',
                'description' => esc_html__("Insert your Bitcoin address here. Leave blank if you want to skip this cryptocurrency", 'cryptapi'),
                'desc_tip' => true,
            ),

            'bch_address' => array(
                'title' => esc_html__('Bitcoin Cash Address', 'cryptapi'),
                'type' => 'text',
                'class' => 'bch_address ca_crypto_hide',
                'description' => esc_html__("Insert your Bitcoin Cash address here. Leave blank if you want to skip this cryptocurrency", 'cryptapi'),
                'desc_tip' => true,
            ),

            'ltc_address' => array(
                'title' => esc_html__('Litecoin Address', 'cryptapi'),
                'type' => 'text',
                'class' => 'ltc_address ca_crypto_hide',
                'description' => esc_html__("Insert your Litecoin address here. Leave blank if you want to skip this cryptocurrency", 'cryptapi'),
                'desc_tip' => true,
            ),

            'eth_address' => array(
                'title' => esc_html__('Ethereum Address', 'cryptapi'),
                'type' => 'text',
                'class' => 'eth_address ca_crypto_hide',
                'description' => esc_html__("Insert your Ethereum address here. Leave blank if you want to skip this cryptocurrency", 'cryptapi'),
                'desc_tip' => true,
            ),

            'usdt_address' => array(
                'title' => esc_html__('USDT Address (ERC-20)', 'cryptapi'),
                'type' => 'text',
                'class' => 'usdt_address ca_crypto_hide',
                'description' => esc_html__("Insert your USDT address here. Leave blank if you want to skip this cryptocurrency", 'cryptapi'),
                'desc_tip' => true,
            ),

            'usdc_address' => array(
                'title' => esc_html__('USDC Address (ERC-20)', 'cryptapi'),
                'type' => 'text',
                'class' => 'usdc_address ca_crypto_hide',
                'description' => esc_html__("Insert your USDC address here. Leave blank if you want to skip this cryptocurrency", 'cryptapi'),
                'desc_tip' => true,
            ),

            'busd_address' => array(
                'title' => esc_html__('BUSD Address (ERC-20)', 'cryptapi'),
                'type' => 'text',
                'class' => 'busd_address ca_crypto_hide',
                'description' => esc_html__("Insert your BUSD address here. Leave blank if you want to skip this cryptocurrency", 'cryptapi'),
                'desc_tip' => true,
            ),

            'pax_address' => array(
                'title' => esc_html__('PAX Address (ERC-20)', 'cryptapi'),
                'type' => 'text',
                'class' => 'pax_address ca_crypto_hide',
                'description' => esc_html__("Insert your PAX address here. Leave blank if you want to skip this cryptocurrency", 'cryptapi'),
                'desc_tip' => true,
            ),

            'tusd_address' => array(
                'title' => esc_html__('TUSD Address (ERC-20)', 'cryptapi'),
                'type' => 'text',
                'class' => 'tusd_address ca_crypto_hide',
                'description' => esc_html__("Insert your TUSD address here. Leave blank if you want to skip this cryptocurrency", 'cryptapi'),
                'desc_tip' => true,
            ),

            'bnb_address' => array(
                'title' => esc_html__('BNB Address (ERC-20)', 'cryptapi'),
                'type' => 'text',
                'class' => 'bnb_address ca_crypto_hide',
                'description' => esc_html__("Insert your BNB address here. Leave blank if you want to skip this cryptocurrency", 'cryptapi'),
                'desc_tip' => true,
            ),

            'link_address' => array(
                'title' => esc_html__('ChainLink Address (ERC-20)', 'cryptapi'),
                'type' => 'text',
                'class' => 'link_address ca_crypto_hide',
                'description' => esc_html__("Insert your ChainLink address here. Leave blank if you want to skip this cryptocurrency", 'cryptapi'),
                'desc_tip' => true,
            ),

            'cro_address' => array(
                'title' => esc_html__('Crypto Coin Address (ERC-20)', 'cryptapi'),
                'type' => 'text',
                'class' => 'cro_address ca_crypto_hide',
                'description' => esc_html__("Insert your Crypto Coin address here. Leave blank if you want to skip this cryptocurrency", 'cryptapi'),
                'desc_tip' => true,
            ),

            'mkr_address' => array(
                'title' => esc_html__('Maker Address (ERC-20)', 'cryptapi'),
                'type' => 'text',
                'class' => 'mkr_address ca_crypto_hide',
                'description' => esc_html__("Insert your Maker address here. Leave blank if you want to skip this cryptocurrency", 'cryptapi'),
                'desc_tip' => true,
            ),

            'nexo_address' => array(
                'title' => esc_html__('NEXO Address (ERC-20)', 'cryptapi'),
                'type' => 'text',
                'class' => 'nexo_address ca_crypto_hide',
                'description' => esc_html__("Insert your NEXO address here. Leave blank if you want to skip this cryptocurrency", 'cryptapi'),
                'desc_tip' => true,
            ),

            'bcz_address' => array(
                'title' => esc_html__('BECAZ Address (ERC-20)', 'cryptapi'),
                'type' => 'text',
                'class' => 'bcz_address ca_crypto_hide',
                'description' => esc_html__("Insert your BECAZ address here. Leave blank if you want to skip this cryptocurrency", 'cryptapi'),
                'desc_tip' => true,
            ),

            'xmr_address' => array(
                'title' => esc_html__('Monero Address', 'cryptapi'),
                'type' => 'text',
                'class' => 'xmr_address ca_crypto_hide',
                'description' => esc_html__("Insert your Monero address here. Leave blank if you want to skip this cryptocurrency", 'cryptapi'),
                'desc_tip' => true,
            ),

            'iota_address' => array(
                'title' => esc_html__('IOTA Address', 'cryptapi'),
                'type' => 'text',
                'class' => 'iota_address ca_crypto_hide',
                'description' => esc_html__("Insert your IOTA address here. Leave blank if you want to skip this cryptocurrency", 'cryptapi'),
                'desc_tip' => true,
            ),

            'trx_address' => array(
                'title' => esc_html__('TRX Address (TRC-20)', 'cryptapi'),
                'type' => 'text',
                'class' => 'trx_address ca_crypto_hide',
                'description' => esc_html__("Insert your TRX address here. Leave blank if you want to skip this cryptocurrency", 'cryptapi'),
                'desc_tip' => true,
            ),

            'payment_timeout' => array(
                'title' => esc_html__('Payment Timeout', 'cryptapi'),
                'type' => 'select',
                'class' => 'payment_timeout',
                'options' => $this->payment_timeout_values,
                'description' => esc_html__("Select a Payment timeout after that you want to update order status", 'cryptapi'),
                'desc_tip' => true,
            ),

            'update_order_req' => array(
                'title' => esc_html__('Update Order into', 'cryptapi'),
                'type' => 'select',
                'class' => 'update_order_req',
                'options' => wc_get_order_statuses(),
                'description' => esc_html__("After Payment timeout in which satuts you want to send your order back", 'cryptapi'),
                'desc_tip' => true,
            ),
        );
    }

    /**
	 * Function is used to reterive the data enterd in woocommerce field
	 *
	 * @since    1.0.0
	 */
    function cryptapi_settings() {
        $this->enabled = $this->get_option('enabled');
        $this->sandbox_mode = $this->get_option('sandbox_mode');
        $this->title = $this->get_option('title');
        $this->description = $this->get_option('description');
        $this->cryptapi_coins = $this->get_option('coins');
        $this->btc_test_address = $this->get_option('btc_test_address');
        $this->payment_timeout = $this->get_option('payment_timeout');
        $this->update_order_req = $this->get_option('update_order_req');

        foreach ( array_keys(  $this->cryptapi_coin_options ) as $coin ) {
            $this->{ $coin . '_address'} = $this->get_option( $coin . '_address' );
        }
    }

    /**
	 * Function is used to make setup for woocommerce fields
	 *
	 * @since    1.0.0
	 */
    function needs_setup()
    {
        if ( empty( $this->cryptapi_coins) || !is_array( $this->cryptapi_coins ) ) return true;

        foreach ( $this->cryptapi_coins as $cryptapi_val ) {
            if ( !empty( $this->{ $cryptapi_val . '_address'} ) ) return false;
        }

        return true;
    }

    /**
	 * Function is used to display payment option field on the public end while order is on checkout page
	 *
	 * @since    1.0.0
	 */
    function payment_fields()
    {
        if( $this->sandbox_mode != 'yes' ) {
        ?>
        <div class="form-row form-row-wide">
            <p><?php echo isset( $this->description ) ? $this->description : ""; ?></p>
            <ul style="list-style: none outside;">
                <?php
                if ( !empty( $this->cryptapi_coins ) && is_array( $this->cryptapi_coins ) ) {
                    foreach ( $this->cryptapi_coins as $cryptapi_val ) {
                        $cryptapi_addr = $this->{ $cryptapi_val . '_address' };
                        if ( !empty( $cryptapi_addr ) ) { ?>
                            <li>
                                <input id="payment_method_<?php echo isset( $cryptapi_val ) ? $cryptapi_val : ""; ?>" type="radio" class="payment_selected_coin input-radio" name="cryptapi_used_coin" value="<?php echo isset( $cryptapi_val ) ? $cryptapi_val : ""; ?>"/>
                                <label for="payment_method_<?php echo isset( $cryptapi_val ) ? $cryptapi_val : ""; ?>" style="display: inline-block;"><?php echo _e('Pay with ', 'cryptapi') . ' ' . isset( $this->cryptapi_coin_options[$cryptapi_val] ) ? $this->cryptapi_coin_options[$cryptapi_val] : ""; ?></label>
                            </li>
                            <?php
                        }
                    }
                } ?>
            </ul>
        </div>
        <?php
        }
        else if( $this->sandbox_mode == 'yes' ) {
            if( isset( $this->btc_test_address ) && !empty( $this->btc_test_address ) ) {
            ?>
            <div class="form-row form-row-wide">
                <p><?php esc_html_e( 'Pay with cryptocurrency (BTC Testnet)', 'cryptapi' ); ?></p>
                <ul style="list-style: none outside; margin-top:5%">
                    <li>
                        <input type="radio" class="payment_selected_coin input-radio" name="cryptapi_used_coin" value="<?php echo 'btc_test'; ?>"/>
                        <label style="display: inline-block;"><?php echo _e('Pay with BTC Testnet', 'cryptapi') ?></label>
                    </li>
                </ul>
            </div>
            <?php
            }
        }
    }

    /**
	 * Function is used to validate fields as per woocommerce default class
	 *
	 * @since    1.0.0
	 */
    function validate_fields()
    {
        return array_key_exists(sanitize_text_field($_POST['cryptapi_used_coin']), $this->cryptapi_coin_options);
    }

    
    /**
	 * Function is used to process payment for the order.
	 *
	 * @since    1.0.0
	 */
    function process_payment($order_id)
    {
        global $woocommerce;

        if( isset( $_POST['cryptapi_used_coin'] ) && !empty( $_POST['cryptapi_used_coin'] ) ) {

            $selected_coin = sanitize_text_field($_POST['cryptapi_used_coin']);
            
            $sel_coin_addr = $this->{$selected_coin . '_address'};

            if ( isset($sel_coin_addr) && !empty( $sel_coin_addr ) && isset($selected_coin) && !empty( $selected_coin ) ) {
    
                $nonce = wp_create_nonce();
    
                $base_url = 'https://api.cryptapi.io/';

                if( $selected_coin == 'btc_test' ) {

                    $base_url = 'https://sandbox.cryptapi.io/';
                    $selected_coin = 'btc';
                    $selected_coin1 = 'btc_test';
                }
    
                $prefix_url = "{$base_url}{$selected_coin}/create/";

                if( in_array( $selected_coin, $this->erc20_tokens) ) {
                    $prefix_url = "{$base_url}erc20/{$selected_coin}/create/";
                }
                $order = new WC_Order( isset( $order_id ) ? $order_id : "" );
    
                $order_received_url = $order->get_checkout_order_received_url();
                
                if( isset( $prefix_url ) && !empty( $prefix_url ) ) {

                    $send_first_url = add_query_arg(array(
                        'address' => isset( $sel_coin_addr ) ? $sel_coin_addr : "" ,
                        'callback' => isset( $order_received_url ) ? $order_received_url : "" ,
                        'invoice' => isset( $order_id ) ? $order_id : "" ,
                        'nonce' => isset( $nonce ) ? $nonce : "" ,
                    ), $prefix_url);

                    $this->initiliaze_url = isset( $send_first_url ) ? $send_first_url : "";

                    $this->verify_callback_url = add_query_arg( array(

                        'invoice' => isset( $order_id ) ? $order_id : "" ,
                        'nonce' => isset( $nonce ) ? $nonce : "" ,
                    ), $order_received_url );
                    
                    $total = $order->get_total('edit');

                    $this->info_url = 'https://api.cryptapi.io/'.$selected_coin.'/info/';

                    if( in_array( $selected_coin, $this->erc20_tokens ) ) {
                        $this->info_url = 'https://api.cryptapi.io/erc20/'.$selected_coin.'/info/';
                    }

                    if( isset( $selected_coin1 ) ) {

                        if( $selected_coin1 == 'btc_test' ) {

                            $this->info_url = 'https://api.cryptapi.io/'.$selected_coin.'/info/';
                        }
                    }
                    
                    $info = $this->cryptapi_use_curl('info');

                    $currency = get_woocommerce_currency();

                    $price = floatval($info->prices->USD);

                    if ( isset( $info->prices->{$currency} ) ) {
                        $price = floatval( $info->prices->{$currency} );
                    }

                    $crypto_total = $this->round_sig( $total / $price, 5 );

                    $min_tx = $this->convert_div( $info->minimum_transaction, $selected_coin );

                    if ($crypto_total < $min_tx) {
                        wc_add_notice(__('Payment error:', 'woocommerce') . __('Value too low, minimum is', 'cryptapi') . ' ' . $min_tx . ' ' . strtoupper($selected_coin), 'error');
                        return null;
                    }
                        
                    $curl_req_for_add = $this->cryptapi_use_curl( 'address' );

                    if( isset( $curl_req_for_add ) && !empty( $curl_req_for_add ) && ( $curl_req_for_add == 'Success' ) ) {
                        $cryptpi_paid = 0;
                        $order->add_meta_data( 'cryptapi_nonce', $nonce );
                        $order->add_meta_data( 'cryptapi_address', $this->address_in );
                        $order->add_meta_data( 'cryptapi_total', $crypto_total );
                        $order->add_meta_data( 'cryptpi_paid', $cryptpi_paid );
                        $order->add_meta_data( 'cryptapi_currency', $selected_coin );
                        $order->add_meta_data( 'payment_timeout', $this->payment_timeout );
                        $order->add_meta_data( 'update_order_req', $this->update_order_req );
                        $order->add_meta_data( 'verify_callback_url', $this->verify_callback_url );
                        $order->add_meta_data( 'prefix_url', $prefix_url );
                        $order->add_meta_data( 'cryptapi_currency_full_name', $this->cryptapi_coin_options[$selected_coin] );
                        $order->add_meta_data( 'currency_uri_first', str_replace(' ','',strtolower($this->cryptapi_coin_options[$selected_coin])) );
                        $order->save_meta_data();
    
                        $order->update_status( 'on-hold', __( 'Awaiting payment', 'woothemes' ) . ': ' . $this->cryptapi_coin_options[$selected_coin] );
                        $woocommerce->cart->empty_cart();
    
                        return array(
                            'result' => 'success',
                            'redirect' => $this->get_return_url( $order )
                        );
                    }
                    else if( isset( $curl_req_for_add ) && !empty( $curl_req_for_add ) && ( $curl_req_for_add == 'error' ) ) {
                        wc_add_notice(__( 'Payment error: ', 'woocommerce' ) . __( $this->error_msg, 'woocommerce' ), 'error' );
                        return null;
                    }
                    else {
                        wc_add_notice(__( 'Payment error: ', 'woocommerce' ) . __( 'Payment Could not be proccessed, try Again', 'woocommerce' ), 'error' );
                        return null;
                    }
                }
            }
        }
        else {
            wc_add_notice(__( 'Payment error: ', 'woocommerce' ) . __( 'Choose Atleast one method to pay', 'woocommerce' ), 'error' );
            return null;
        }
    }

    /**
	 * Function is used to calculate rounds of number for currencies.
	 *
	 * @since    1.0.0
	 */
    private function round_sig($number, $sigdigs = 5)
    {
        $multiplier = 1;
        while ($number < 0.1) {
            $number *= 10;
            $multiplier /= 10;
        }
        while ($number >= 1) {
            $number /= 10;
            $multiplier *= 10;
        }
        return round($number, $sigdigs) * $multiplier;
    }

    /**
	 * Function is used send and retrieve data from the server by using curl
	 *
	 * @since    1.0.0
	 */
    private function cryptapi_use_curl( $ca_for )
    {
        $ca_curl = curl_init();
        curl_setopt( $ca_curl, CURLOPT_HEADER, false );
        curl_setopt( $ca_curl, CURLOPT_HTTPHEADER, array( 'Accept: application/json', 'Accept-Language: en_US' ) );
        curl_setopt( $ca_curl, CURLOPT_VERBOSE, 1 );
        curl_setopt( $ca_curl, CURLOPT_TIMEOUT, 30 );
        if( $ca_for == 'address' ) {
            curl_setopt( $ca_curl, CURLOPT_URL, $this->initiliaze_url );
        }
        else if( $ca_for == 'info' ) {
            curl_setopt( $ca_curl, CURLOPT_URL, $this->info_url );
        }
        curl_setopt( $ca_curl, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ca_curl, CURLOPT_SSLVERSION, 6 );
        $ca_curl_response = curl_exec( $ca_curl );
        curl_close( $ca_curl );
        if( $ca_for == 'address' ) {
            $ca_curl_response_array = json_decode( $ca_curl_response, true );
            if( isset( $ca_curl_response_array['status'] ) && !empty( $ca_curl_response_array['status'] ) ) {
                if( $ca_curl_response_array['status'] == 'success' ) {
                    $this->address_in = isset( $ca_curl_response_array['address_in'] ) ? $ca_curl_response_array['address_in'] : "";
                    $this->address_out = isset( $ca_curl_response_array['address_out'] ) ? $ca_curl_response_array['address_out'] : "";
                    $this->callback_url = isset( $ca_curl_response_array['callback_url'] ) ? $ca_curl_response_array['callback_url'] : "";
                    $this->priority = isset( $ca_curl_response_array['priority'] ) ? $ca_curl_response_array['priority'] : "";
                    return 'Success';
                }
                else if( $ca_curl_response_array['status'] == 'error' ) {
                    $this->error_msg = isset( $ca_curl_response_array['error'] ) ? $ca_curl_response_array['error'] : "" ;
                    return 'error';
                }
                else {
                    return 'False';
                }
            }
        }
        else if( $ca_for == 'info' ) {
            return json_decode( $ca_curl_response );
        }
    }

    /**
	 * Function is used to calculate division of the values.
	 *
	 * @since    1.0.0
	 */
    private function convert_div( $val, $coin ) {
        return $val / WC_CryptAPI_Gateway::$COIN_MULTIPLIERS[$coin];
    }
}