(function( $ ) {
	'use strict';

	/**
	 * All of the code for your admin-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a partioccular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	$(document).ready(function() {
		
		$(document).find('.sandbox_warning_class').parent().parent().parent().parent().hide();
		$(document).find('.sandbox_warning_class').parent().parent().parent().siblings().hide();
		$(document).find('.btc_test_address').parent().parent().parent().hide();
		$(document).find('.sandbox_warning_class').parent().addClass('sandbox_warning_class_label');
		var ca_crypto_all_classes = [];
		$(document).find('.ca_crypto_hide').each(function(){
			var ca_crypto_classes = $(this).attr('class');
			ca_crypto_classes = ca_crypto_classes.replace('input-text regular-input ','');
			ca_crypto_classes = ca_crypto_classes.replace(' ca_crypto_hide','');
			ca_crypto_all_classes.push(ca_crypto_classes);
		})
		$(ca_crypto_all_classes).each(function(index,val){
			$(document).find('.'+val).parent().parent().parent().hide();
		})
		var ca_crypto_selected = $(document).find('.cryptapi_multiselect').val();
		$(ca_crypto_selected).each(function(index,val){
			$(document).find('.'+val+'_address').parent().parent().parent().show();
		})
		if( $(document).find('.enable_sandbox').is(':checked') ) {
			$(document).find('.sandbox_warning_class').parent().parent().parent().parent().show();
			$(document).find('.ca_crypto_hide').parent().parent().parent().hide();
			$(document).find('.cryptapi_multiselect').parent().parent().parent().hide();
			$(document).find('.btc_test_address').parent().parent().parent().show();
		}
		$(document).on('click','.enable_sandbox',function(){
			if( $(document).find('.enable_sandbox').is(':checked') ) {
				$(document).find('.sandbox_warning_class').parent().parent().parent().parent().show();
				$(document).find('.ca_crypto_hide').parent().parent().parent().hide();
				$(document).find('.cryptapi_multiselect').parent().parent().parent().hide();
				$(document).find('.btc_test_address').parent().parent().parent().show();
			}
			else {
				$(document).find('.sandbox_warning_class').parent().parent().parent().parent().hide();
				$(document).find('.cryptapi_multiselect').parent().parent().parent().show();
				$(document).find('.btc_test_address').parent().parent().parent().hide();
				var ca_crypto_selected_on_click = $(document).find('.cryptapi_multiselect').val();
					$(ca_crypto_selected_on_click).each(function(index,val){
					$(document).find('.'+val+'_address').parent().parent().parent().show();
				})
			}
		})
		$(document).on('click','.cryptapi_multiselect',function(){
			var ca_crypto_selected_on_click = $(this).val();
			$(ca_crypto_all_classes).each(function(index,val){
				$(document).find('.'+val).parent().parent().parent().hide();
			})
			$(ca_crypto_selected_on_click).each(function(index,val){
				$(document).find('.'+val+'_address').parent().parent().parent().show();
			})
		})
	})

})( jQuery );
